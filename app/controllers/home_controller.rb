class HomeController < ApplicationController

  # so that respond_with knows which formats are
  # allowed in each of the individual actions
  respond_to :html, :json

  def index
    @users = User.all
    @user = current_user if current_user.present?
    respond_to do |format|
      format.html
      format.json { render :json => Interest.json_tree }
    end
  end

end
