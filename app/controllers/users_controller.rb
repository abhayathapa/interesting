class UsersController < ApplicationController
  before_filter :authenticate_user!

  # def index
  #   @users = User.all
  # end

  def show
    if params[:id]
      @user = User.find(params[:id])
    else
      @user = current_user
    end
    @interests = @user.interests
  end

  def update_interests
    @user = User.find(params[:id])
    @user.raw_interests
    redirect_to user_path(@user)
  end
end
