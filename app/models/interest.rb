class Interest < ActiveRecord::Base
  attr_accessible :name

  acts_as_tree # https://github.com/mceachen/closure_tree

  # has_and_belongs_to_many :user, join_table: "table_name", foreign_key: "user_id"
  has_many :user_interests
  has_many :users, through: :user_interests

  def self.json_tree
    {'name' => 'Socialize', 'children' => self.name_and_children(Interest.hash_tree)}
  end


  def self.name_and_children(hash)
    hash.inject([]) do |result_array, (interest, children_hash)|
      result = {'name' => interest.name }

      result['children'] = if children_hash.blank?
        []
      else
        children_hash.inject([]) do |result_array_2, child_array|
          result_array_2 += name_and_children(child_array.first => child_array.last)
        end
      end
      result_array << result
      result_array
    end
  end

end

# == Schema Information
#
# Table name: interests
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

