class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  attr_accessible :provider, :uid, :oauth_token, :latitude, :longitude

  # Setup accessible (or protected) attributes for your model
  attr_accessible :role_ids, :as => :admin
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :facebook_id, :image
  has_many :interests, through: :user_interests
  has_many :user_interests

  geocoded_by :current_sign_in_ip   # can also be an IP address
  before_save :geocode, if: lambda{|user| user.current_sign_in_ip_changed? }
  after_initialize :geocode, if: lambda{|user| user.latitude.nil? || user.longitude.nil? }
  acts_as_gmappable :process_geocoding => false, :lat => 'latitude', :lon => 'longitude'

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil, location)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = User.create(name:auth.extra.raw_info.name,
       provider:auth.provider,
       uid:auth.uid,
       email:auth.info.email,
       password:Devise.friendly_token[0,20],
       image: auth['info']['image'],
       oauth_token: auth.credentials.token
       )
    end
    # if auth['extra']['raw_info']
    # end
    user
  end

  def raw_interests
    @graph = Koala::Facebook::API.new(oauth_token)
    ["movies", "music", "television","games","books"].each do |int_list|
      @graph.get_connections('me',int_list).each do |interest_name|
        parent = Interest.find_or_create_by_name(interest_name["category"])
        interest = parent.children.find_or_create_by_name(interest_name["name"])
        UserInterest.find_or_create_by_interest_id_and_user_id(interest.id, self.id)
      end
    end
  end

  def initial_interests(auth)
    ["sports",'favorite_athletes', 'favorite_teams','languages'].each do |interest_list|
      auth[interest_list].map(&:name).each do |interest_name|
        parent = Interest.find_or_create_by_name(interest_list)
        interest = parent.children.find_or_create_by_name(interest_name)
        UserInterest.find_or_create_by_interest_id_and_user_id(interest.id, self.id)
      end if auth[interest_list]
    end
  end

  def people_of_interest(distance)
    my_interests = self.interests.map(&:id)
    self.nearbys(distance.to_i).joins(:interests).where{interests.id >> my_interests }
  end

  def gmaps4rails_infowindow
      "<img src=\"#{self.image}\"> <a href=\"#{self.id}\">#{self.name}</a>"
      # str1 = image_tag self.image
      # str2 = link_to self.name, user_path(self)
      # str1 + str2
  end

  def recommended_friends
     my_interests = interests.map(&:id)
     my_friends= []
     my_interests.each do |my_interest|
      my_interest_users = my_interest.map(&:id) - [self.id]
      my_interest_users.each do |fren|
        my_friends << {
          "friend" => fren.id,
          "interest" => my_interest.name
         }
       end
     end
  end
end

class NilClass
  def [](* args)
    nil
  end
  def each(*args)
    nil
  end
end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string(255)
