Rails3BootstrapDeviseCancan::Application.routes.draw do
  # authenticated :user do
  #   root :to => 'users#show'
  # end
  root :to => "home#index"
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  resources :users do
    member do
      get 'update_interests'
    end
  end

  match "/index" => "home#index"
end
