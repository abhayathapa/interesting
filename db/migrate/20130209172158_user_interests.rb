class UserInterests < ActiveRecord::Migration
  def up
    create_table :user_interests do |t|
      t.integer :interest_id
      t.integer :user_id
      t.integer :weight
    end
    add_index :user_interests, :interest_id
    add_index :user_interests, :user_id
  end

  def down
    op_table :user_interests
  end
end
